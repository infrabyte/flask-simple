from ma import ma
from models.StoreModel import StoreModel
from models.ItemModel import ItemModel
from schemas.ItemSchema import ItemSchema


class StoreSchema(ma.SQLAlchemyAutoSchema):
    items = ma.Nested(ItemSchema, many=True)

    class Meta:
        model = StoreModel
        load_instance = True
        include_fk = True