from ma import ma
from models.ItemModel import ItemModel
from models.StoreModel import StoreModel


class ItemSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ItemModel
        load_instance = True
        load_only = ("store",)
        include_fk= True