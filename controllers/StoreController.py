from flask import request
from flask_restful import Resource

from models.StoreModel import StoreModel
from schemas.StoreSchema import StoreSchema

STORE_NOT_FOUND = "Store not found."
STORE_ALREADY_EXISTS = "Store '{}' Already exists."

store_schema = StoreSchema()
store_list_schema = StoreSchema(many=True)

class Store(Resource):
    def get(self, id):
        store_data = StoreModel.find_by_id(id)
        if store_data:
            return store_schema.dump(store_data)
        return {'message': STORE_NOT_FOUND}, 404

    def delete(self, id):
        store_data = StoreModel.find_by_id(id)
        if store_data:
            store_data.delete_from_db()
            return {'message': "Store Deleted successfully"}, 200
        return {'message': STORE_NOT_FOUND}, 404


class StoreList(Resource):
    def get(self):
        return store_list_schema.dump(StoreModel.find_all()), 200

    def post(self):
        store_json = request.get_json()
        name = store_json['name']
        if StoreModel.find_by_name(name):
            return {'message': STORE_ALREADY_EXISTS.format(name)}, 400

        store_data = store_schema.load(store_json)
        store_data.save_to_db()

        return store_schema.dump(store_data), 201 