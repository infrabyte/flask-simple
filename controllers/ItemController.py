from flask import request
from flask_restful import Resource

from models.ItemModel import ItemModel
from schemas.ItemSchema import ItemSchema

ITEM_NOT_FOUND = "Item not found."

item_schema = ItemSchema()
item_list_schema = ItemSchema(many=True)

class Item(Resource):

    def get(self, id):
        item_data = ItemModel.find_by_id(id)
        if item_data:
            return item_schema.dump(item_data)
        return {'message': ITEM_NOT_FOUND}, 404

    def delete(self,id):
        item_data = ItemModel.find_by_id(id)
        if item_data:
            item_data.delete_from_db()
            return {'message': "Item Deleted successfully"}, 200
        return {'message': ITEM_NOT_FOUND}, 404

    def put(self, id):
        item_data = ItemModel.find_by_id(id)
        item_json = request.get_json()

        if item_data:
            item_data.price = item_json['price']
            item_data.name = item_json['name']
        else:
            item_data = item_schema.load(item_json)

        item_data.save_to_db()
        return item_schema.dump(item_data), 200


class ItemList(Resource):
    def get(self):
        return item_list_schema.dump(ItemModel.find_all()), 200

    def post(self):
        item_json = request.get_json()
        item_data = item_schema.load(item_json)
        item_data.save_to_db()

        return item_schema.dump(item_data), 201