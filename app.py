from flask import Flask, Blueprint, jsonify
from flask_restful import Api
from ma import ma
from db import db
from marshmallow import ValidationError

app = Flask(__name__)
api = Api(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:wil99@localhost:3306/tododb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True

@app.before_first_request
def create_tables():
    db.create_all()

@app.route('/')
def index():
    return "Hola, bienvenidos a mi api"

# @api.errorhandler(ValidationError)
# def handle_validation_error(error):
#     return jsonify(error.messages), 400



from controllers.ItemController import Item, ItemList
api.add_resource(Item, '/item/<int:id>')
api.add_resource(ItemList, '/items')

from controllers.StoreController import Store, StoreList
api.add_resource(Store, '/store/<int:id>')
api.add_resource(StoreList, '/stores')

if __name__ == "__main__":
    db.init_app(app)
    ma.init_app(app)
    app.run(debug=True)